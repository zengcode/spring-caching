package zengcode.medium.com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import zengcode.medium.com.service.IPersonRepository;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@SpringBootApplication
@EnableCaching
public class Application {

    @Autowired
    public IPersonRepository personRepository;

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
        Application application = ctx.getBean(Application.class);

        System.out.println("Before caching......");
        for(int i = 1; i <= 5; i++) {
            Instant start = Instant.now();
            System.out.println("===> " + application.personRepository.getById(i));
            Instant end = Instant.now();
            System.out.println("total execution times ========> " + ChronoUnit.MILLIS.between(start, end));
        }

        System.out.println("===============================");
        System.out.println("After caching......");
        for(int i = 1; i <= 5; i++) {
            Instant start = Instant.now();
            System.out.println("===> " + application.personRepository.getById(i));
            Instant end = Instant.now();
            System.out.println("total execution times ========> " + ChronoUnit.MILLIS.between(start, end));
        }
    }

}
