package zengcode.medium.com.service;


import zengcode.medium.com.model.Person;

public interface IPersonRepository {

    public Person getById(int id);
}
