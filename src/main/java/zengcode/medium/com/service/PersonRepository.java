package zengcode.medium.com.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import zengcode.medium.com.model.Person;

import java.util.UUID;

@Repository
public class PersonRepository implements IPersonRepository {

    @Override
    @Cacheable("persons")
    public Person getById(int id) {
        //simulate delay for 3 secs
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new Person(id, "Chiwa " + UUID.randomUUID().toString());
    }
}
